<?php
include('connection.php');
 
if(isset($_REQUEST["term"])){
    // Prepare a select statement
    $sql = "SELECT * FROM skills WHERE skill LIKE ?";
    
    if($stmt = mysqli_prepare($link, $sql)){
        // Bind variables to the prepared statement as parameters
        mysqli_stmt_bind_param($stmt, "s", $param_term);
        
        // Set parameters
        $param_term = '%' . $_REQUEST["term"] . '%';
        
        // Attempt to execute the prepared statement
        if(mysqli_stmt_execute($stmt)){
            $result = mysqli_stmt_get_result($stmt);
            
            // Check number of rows in the result set
            if(mysqli_num_rows($result) > 0){
                //aanmaken van een array waarin alleen values in komen die nog niet erin zijn gestopt om tegen te gaan dat er meerdere 'UX designs worden getoont'
                $loopedArray = array();

                // Fetch result rows as an associative array
                while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
                    if (in_array($row["skill"], $loopedArray)) {
                        
                    }
                    else{
                         echo "<p>" . $row["skill"] . "</p>";
                         array_push($loopedArray, $row["skill"]);
                    }
                }

            } else{
                echo "<p class='noMatch'>No matches found</p>";
            }
        } else{
            echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
        }
    }
     
    // Close statement
    mysqli_stmt_close($stmt);
}
 
// close connection
mysqli_close($link);
?>


