<?php 
    include('connection.php');

    session_start();

    //check of gebruiker van de project pagina komt
    $_SESSION['vanProjPage'] =  true;


    //ophalen data van aangeklikt projects, in de vorm van een ID
    $projID = $_GET["proj"];

    //als er proj data is verzonden vanaf vorige pagina
    if(isset($_GET["proj"])){
        //element ID opzoeken in de projecten database tabel
        $sql = "SELECT * FROM projects WHERE p_id = $projID";

        //Fetch results COUNT PROJECT FILTERS
        $result=mysqli_query($link, $sql);

        while($row=mysqli_fetch_array($result))
        {   
           $name = $row['p_name']; 
           $location = $row['p_location']; 
           $description = $row['p_description']; 
           $owner = $row['p_owner'];
           $fontys = $row['p_fontys'];
           $other = $row['p_other'];
           $volunt = $row['p_voluntary'] ;
           $paid = $row['p_paid'];
           $skilldesc = $row['p_skilldescription'];
         
        }   

            }else{
                echo 'noteset';
            }

?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://unpkg.com/popper.js"></script>
  <link rel="stylesheet" href="https://use.typekit.net/mqf0bdz.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
   <link rel="stylesheet" type="text/css" href="style2.css">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script type="text/javascript">
</script>
</head>
<body>
    <div class="cont1400">
        <div class="header">
            <div class="shadowhider">
                <div class="logoCont">
                    <img src="images/logo6.png" height="52px">
                </div>
                <!-- end of logoCont -->

                <div class="menuCont">
                    <img class="menuIcon" src="images/icons/Component 25 – 2.svg"/>
                </div>
                <!-- end of menuCont -->
                <div class="twoMenu">
                        <a class="twoMenuSec" href="peoplesearch.php"><span>People Search</span>
                    <div id="Component_15___157">
                        <svg class="Rectangle_640_A24_Rectangle_14">
                            <rect id="Rectangle_640_A24_Rectangle_14" rx="3" ry="3" x="0" y="0" width="100%" height="7">
                            </rect>
                        </svg>
                        <svg class="Rectangle_645_A24_Rectangle_15">
                            <rect id="Rectangle_645_A24_Rectangle_155" rx="1.5" ry="1.5" x="0" y="0" width="100%" height="3">
                            </rect>
                        </svg>
                    </div>
                        </a>
                        <a class="twoMenuSec activeSearch" href="projectsearch.php"><span>Project Search</span>
                                <div id="Component_15___157">
            <svg class="Rectangle_640_A24_Rectangle_14">
                <rect id="Rectangle_640_A24_Rectangle_14" rx="3" ry="3" x="0" y="0" width="100%" height="7">
                </rect>
            </svg>
            <svg class="Rectangle_645_A24_Rectangle_15">
                <rect id="Rectangle_645_A24_Rectangle_15" rx="1.5" ry="1.5" x="0" y="0" width="100%" height="3">
                </rect>
            </svg>
        </div></a>
                </div>
                <!-- end of  twoMenu-->
            </div>
            <!-- end of shadowheader -->
            <div class="shadowheader">

            </div>
            <!-- end of shadowheader -->
        </div>
        <!-- end of  header-->

        <div class="invisHeader">

        </div>
        <!-- end of  -->
        <!--  -->
        <div class="backToResults">
            <a href="unite.php" class="btn btn-primary backButton">
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 22" style="    width: 1.5rem;
    margin-right: 0.2rem;">
  <defs id="defs3051">
    <style type="text/css" id="current-color-scheme">
      .ColorScheme-Text {
        color:#707070;
      }
      </style>
  </defs>
 <path 
    style="fill:currentColor;fill-opacity:1;stroke:none" 
    d="m14.292969 3l-6.125 6.125-1.875 1.875 1.875 1.875 6.125 6.125.707031-.707031-6.125-6.125-1.167969-1.167969 1.167969-1.167969 6.125-6.125-.707031-.707031"
    class="ColorScheme-Text"
    />  
</svg>Results</a>
        </div>
        <!-- contact details returnToResults-->

<div class="projectDetails">
    <div class="titleCont">
        <h2><?php echo $name;?></h2>
    </div>
    <!-- end of title -->

    <div class="columnCont3">

        <div class="column">
            <div class="columnTitle">
                <p>Project description</p> 
            </div>
            <!-- end of columnTitle -->
            <div class="columnText">
                <p><?php echo $description;?><br><br><?php echo $description;?><br><br><?php echo $description;?><br><br></p>
            </div>
            <!-- end of columnText -->
        </div>
        <!-- end of column -->

        <div class="column">
            <div class="columnTitle">
                <p>Needed tasks description</p>
            </div>
            <!-- end of columnTitle -->
            <div class="columnText">
                <p><?php echo $skilldesc;?><br><br><?php echo $skilldesc;?><br><br><?php echo $skilldesc;?><br><br></p>
            </div>
            <!-- end of columnText -->
        </div>
        <!-- end of column -->


         <div class="column">
            <div class="columnTitle">
                <p>Project type</p>
            </div>
            <!-- end of columnTitle -->
            <div class="columnText">




                        <div class="card-footer">
                            <img class="iconn iconn1" src="images/icons/locator.svg"></img>
                            <!-- display project skills -->
                            <a href="#" class="btn btnn btn-primary">
                            <?php echo $location;?></a>
                           
                        </div>
                        <div class="card-footer">
                            <img class="iconn iconn1" src="images/icons/locator.svg"></img>
                            <!-- display project skills -->
                            <?php
                            $sql2 = "SELECT skill FROM skills WHERE p_id = $projID";
                            //Fetch results 
                            $result2=mysqli_query($link, $sql2);

                            while($row2=mysqli_fetch_array($result2))
                            {
                                echo '<a href="#" class="btn btnn btn-primary">' . $row2['skill'] . '</a>';
                            }
                            ?>
                        </div>

                        <div class="card-footer">
                            <img class="iconn iconn1" src="images/icons/locator.svg"></img>
                         <?php if($fontys == 1){echo '<a href="#" class="btn btnn btn-primary">Fontys project</a>';}
                         if($other == 1){echo '<a href="#" class="btn btnn btn-primary">non-Fontys project</a>';}
                         if($volunt == 1){echo '<a href="#" class="btn btnn btn-primary">voluntary</a>';}
                         if($paid == 1){echo '<a href="#" class="btn btnn btn-primary">paid</a>';}
                         ?>

                        </div>


                        <div class="card-footer contactDetails">
                            <a href="#" class="btn btnn btn-primary contactButton">Contact Details</a>
                        </div>
                        <!-- contact details -->
        </div> 
            </div>
            <!-- end of columnText -->
        </div>
        <!-- end of column -->
    </div>
    <!-- end of  3columnCont-->
</div>
<!-- end of projectDetails -->
    </div>
    </div>
    <!-- end of cont1400 -->
<script>
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>

</body>
</html>

