<?php 
    include('connection.php');

    //session set
    session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://unpkg.com/popper.js"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="style.css">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script type="text/javascript">

// array voor geselecteerde skill filters
var selectedFilters = [];

//chechbox variabelen van project filers
var fontys = 0;
var nonFontys = 0;
var voluntary = 0;
var paid = 0;


$(document).ready(function(){
    //element voor ajax conten
    var projectResults = $(".projectResults");

    //starten op page 1
    var pageID = 1;

    //Deze onkeyup roept met jquery een AJAX call op en geeft het variabe 'inputVal' mee dat de waarde van het input veld is. De html data die die terug krijgt wordt toegewezen aan het element 'resultDropdown'. 
    $('.search-box input[type="text"]').on("keyup input", function(){
        var inputVal = $(this).val();
        var resultDropdown = $(this).siblings(".result");

        //als inputveld een length heeft, stuur de waarde dan naar de php file
        if(inputVal.length){
            $.get("dropdown.php", {term: inputVal}).done(function(data){
                //toon de data in de browser
                resultDropdown.html(data);
            });
        } else{
            resultDropdown.empty();
        }
    });
    
    // click listener van de voorgestelde zoekresultaten van de vorige functie. Bij een click wordt value van het skill veld meegeven en worden er projecten ingeladen waarbij die skill n de database staat.
    $(document).on("click", ".result p", function(){
        var huidigeSkill =  $(this).html();

        //return wanneer het NO matches found resultaat wordt aangeklikt.
        if(huidigeSkill == "No matches found"){

        }
        else{  
        //add filter functie wordt aangeroepen, die voegt een HTML filter element toe onder de zoekbalk zodat mensen zien op welke woorden de huidige resultaten worden gefilterd.
        addFilter(huidigeSkill);

        //zet alle aangeklikte filters in een array
        selectedFilters.push(huidigeSkill);

        //huidige filters array wordt omgezet in JSON om mee gegeven te worden voor AJAX call
        var jsonString = JSON.stringify(selectedFilters);

        //AJAX call voor poject results op basis van filterarray
        $.get("filterSearch.php", {filters: jsonString, page: pageID, fontys: fontys, nonFontys: nonFontys, voluntary: voluntary, paid: paid}).done(function(data){
                // tonen van data in html
                projectResults.html(data);
            });

        //leegmaken inputveld
        $(this).parents(".search-box").find('input[type="text"]').val("");
        $(this).parent(".result").empty();
        }
    });

    //aanmaken van een clickbaar filter element onder de zoekbalk
    function addFilter(filterName){
        $(".filterBox").append("<div class='filterItem'><p>" + filterName +  "<img src='images/icons/cross2.svg' class='cross'></img></p></div>");
    };

    // weghalen filter element uit de huidigeFilters array, ververst de resultaten middels AJAX
    $(document).on("click", ".filterItem", function(){
        var filterWaarde = $(this, "php").text(); 

        selectedFilters.splice($.inArray(filterWaarde, selectedFilters),1);

        //JSON encoding
        var jsonString = JSON.stringify(selectedFilters);
        var pageID = 1;

        //AJAX call voor poject results
        $.get("filterSearch.php", {filters: jsonString, page: pageID, fontys: fontys, nonFontys: nonFontys, voluntary: voluntary, paid: paid}).done(function(data){
             // Display the returned data in browser
             projectResults.html(data);
        });

        //clear input fields
        $(this).remove();

    });

    //alle projecten tonen eerste keer opstarten site
    function allProjectShow(){
         //JSON encoding
        var jsonString = JSON.stringify(selectedFilters);

        //AJAX call voor poject results
        $.get("filterSearch.php", {filters: jsonString, page: pageID, fontys: fontys, nonFontys: nonFontys, voluntary: voluntary, paid: paid}).done(function(data){
             // Display the returned data in browser
             projectResults.html(data);
        });
    }

    
    <?php 
    //check of gebruiker van de project pagina komt
    if($_SESSION['vanProjPage'] == true){
        echo 'showSessionProjects();';
        $_SESSION['vanProjPage'] =  false;
    }
    else{
        echo 'allProjectShow();';
    }
    ?>


    //results call alle projecten
    function allProjects(filterName){
        $(".filterBox").append("<div class='filterItem'><p>" + filterName +  "<img src='images/icons/cross2.svg' class='cross'></img></p></div>");
    };


//toggle show van filter menu
$( ".dropdown-toggle" ).click(function() {
    $(".dropdown-menuu").show();
    $(document).mouseup(function(e){
        var container = $(".dropdown-menuu");

        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0) 
        {
            container.hide();
        }
    });
});


//wanneer geklikt wordt op een pagination pagina nummer
$(document).on('click', '.page-link', function(){
    //haal page nummer uit de html
    var pageNum = $(this).html();

    //JSON encoding
    var jsonString = JSON.stringify(selectedFilters);

    //AJAX call voor poject results
    $.get("filterSearch.php", {filters: jsonString, page: pageNum, fontys: fontys, nonFontys: nonFontys, voluntary: voluntary, paid: paid}).done(function(data){
        // toon nieuwe project results in browser
        projectResults.html(data);
    });
});


//redirect naar dynamische project pagina bij klik op project card
$(document).on('click', '.cardd', function(){
    //haal page nummer uit de html
    var projectID = $(this).id();

    //JSON encoding
    var jsonString = JSON.stringify(selectedFilters);

    //AJAX call voor poject results
    $.get("filterSearch.php", {filters: jsonString, page: pageNum, fontys: fontys, nonFontys: nonFontys, voluntary: voluntary, paid: paid}).done(function(data){
        // Display the returned data in browser
        projectResults.html(data);
    });
});


    $(".dropdown-item").click(function(){ 
    var geklikteFilter = $(this).find("span").html();
    var huidigeCheckbox = $(this).find(".checkbox");

    if(geklikteFilter == "Fontys projects"){
        if(fontys == 0){
            huidigeCheckbox.toggleClass("checkboxActive");
            fontys = 1;
            toggleFiltersCall();
        }else{
            huidigeCheckbox.toggleClass("checkboxActive");
            fontys = 0;
            toggleFiltersCall();
        }
    }

    if(geklikteFilter == "Non-Fontys projects"){
        if(nonFontys == 0){
            huidigeCheckbox.toggleClass("checkboxActive");
            nonFontys = 1;
            toggleFiltersCall();
        }else{
            huidigeCheckbox.toggleClass("checkboxActive");
            nonFontys = 0;
            toggleFiltersCall();
        }
    }

    if(geklikteFilter == "Voluntary"){
        if(voluntary == 0){
            huidigeCheckbox.toggleClass("checkboxActive");
            voluntary = 1;
            toggleFiltersCall();
        }else{
            huidigeCheckbox.toggleClass("checkboxActive");
            voluntary = 0;
            toggleFiltersCall();
        }
    }

    if(geklikteFilter == "Paid"){
        if(paid == 0){
            huidigeCheckbox.toggleClass("checkboxActive");
            paid = 1;
            toggleFiltersCall();
        }else{
            huidigeCheckbox.toggleClass("checkboxActive");
            paid = 0;
            toggleFiltersCall();
        }
    }

    function  toggleFiltersCall(){
        //JSON encoding
        var jsonString = JSON.stringify(selectedFilters);


        //AJAX call voor poject results
        $.get("filterSearch.php", {filters: jsonString, page: pageID, fontys: fontys, nonFontys: nonFontys, voluntary: voluntary, paid: paid}).done(function(data){
            // Display the returned data in browser
            projectResults.html(data);
        });
    }



    });


});



// SHOW PROJECETS FROM SESSION DATA
function showSessionProjects(){
    var projectResults2 = $(".projectResults");

    var myJsonArray = <?php echo json_encode($_SESSION['skillArray']) ?>;
    console.log('jsonar' + myJsonArray);

    var pageNum = <?php echo $_SESSION['page']; ?>;
     var fontyss = <?php echo $_SESSION['fontys'];?>;
     var nonFontyss = <?php echo $_SESSION['nonFontys'];?>;
     var paidd = <?php echo $_SESSION['paid'];?>;
     var voluntaryy = <?php echo $_SESSION['voluntary'];?>;

    console.log('pagenumer' + pageNum);

    // var myStringArray = JSON.parse(myJsonArray);
     console.log('parsed array' + myJsonArray);

    console.log('filters' + selectedFilters);
    console.log('fontysss' + fontyss + nonFontyss + paidd + voluntaryy);

    //checkboxes project filters en bijhorende array updaten

        if(fontyss == 1){
            $('.cb1').toggleClass("checkboxActive");
            fontys = 1;
        }

        if(nonFontyss == 1){
            $('.cb2').toggleClass("checkboxActive");
            nonFontys = 1;
        }
    
        if(voluntaryy == 1){
            $('.cb3').toggleClass("checkboxActive");
            voluntary = 1;
        }
    
        if(paidd == 1){
            $('.cb4').toggleClass("checkboxActive");
            paid = 1;
        }
    

    var arrayLength = myJsonArray.length
    for (var i = 0; i < arrayLength; i++){
        console.log(myJsonArray[i]);
        //Do something
         $(".filterBox").append("<div class='filterItem'><p>" + myJsonArray[i] +  "<img src='images/icons/cross2.svg' class='cross'></img></p></div>");
         selectedFilters.push(myJsonArray[i]);
    }

    console.log('filters' + selectedFilters);


    //JSON encoding
    var jsonString = JSON.stringify(myJsonArray);

    //AJAX call voor poject results
    $.get("filterSearch.php", {filters: jsonString, page: pageNum, fontys: fontys, nonFontys: nonFontys, voluntary: voluntary, paid: paid}).done(function(data){
        // Display the returned data in browser
        projectResults2.html(data);
    });
};




</script>
</head>
<body>
    <div class="cont1400">
              <div class="header">
            <div class="shadowhider">
                <div class="logoCont">
                    <img src="images/logo6.png" height="52px">
                </div>
                <!-- end of logoCont -->

                <div class="menuCont">
                    <img class="menuIcon" src="images/icons/Component 25 – 2.svg"/>
                </div>



                <!-- end of menuCont -->
                <div class="twoMenu">
                        <a class="twoMenuSec" href="peoplesearch.php"><span>People Search</span>
                    <div id="Component_15___157">
                        <svg class="Rectangle_640_A24_Rectangle_14">
                            <rect id="Rectangle_640_A24_Rectangle_14" rx="3" ry="3" x="0" y="0" width="100%" height="7">
                            </rect>
                        </svg>
                        <svg class="Rectangle_645_A24_Rectangle_15">
                            <rect id="Rectangle_645_A24_Rectangle_155" rx="1.5" ry="1.5" x="0" y="0" width="100%" height="3">
                            </rect>
                        </svg>
                    </div>
                        </a>
                        <a class="twoMenuSec activeSearch" href="projectsearch.php"><span>Project Search</span>
                                <div id="Component_15___157">
            <svg class="Rectangle_640_A24_Rectangle_14">
                <rect id="Rectangle_640_A24_Rectangle_14" rx="3" ry="3" x="0" y="0" width="100%" height="7">
                </rect>
            </svg>
            <svg class="Rectangle_645_A24_Rectangle_15">
                <rect id="Rectangle_645_A24_Rectangle_15" rx="1.5" ry="1.5" x="0" y="0" width="100%" height="3">
                </rect>
            </svg>
        </div></a>
                </div>
                <!-- end of  twoMenu-->
            </div>
            <!-- end of shadowheader -->
            <div class="shadowheader">

            </div>
            <!-- end of shadowheader -->
        </div>
        <!-- end of  header-->

        <div class="invisHeader">

        </div>
        <!-- end of  -->
        <!-- end of  -->
<div class="search-boxContParent">
        <div class="search-boxCont">
            <div class="search-box clearfix">
                <input type="text" autocomplete="off" placeholder="Zoek hier naar skills" />
                <div class="result clearfix"></div>
            </div>
                    <div class="btn-group ">
                  <button class="btn btn-secondary btn-sm dropdown-toggle dropdownBtn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Toon alleen...
                  </button>
                  <div class="dropdown-menuu ">

                   <div class="dropdown-item" ><div class="checkbox cb1"></div><span class="checkItem">Fontys projects</span></div> 
                      <div class="dropdown-item" ><div class="checkbox cb2"></div><span class="checkItem">Non-Fontys projects</span></div>
                       <div class="dropdown-item" ><div class="checkbox cb3"></div><span class="checkItem">Voluntary</span></div>
                        <div class="dropdown-item" ><div class="checkbox cb4"></div><span class="checkItem">Paid</span></div>
                  </div>
            </div>

            <div class="filterBox">
            </div>
            <!-- end of filterBox -->
        </div>
        <!-- end of  search-boxCont-->
    </div>
<!-- end of search-boxContParen --> 

         <div class="projectResults clearfix card-deck cstmCard-deck">

        </div>

    </div>
    </div>
    <!-- end of cont1400 -->

<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
</body>
</html>

