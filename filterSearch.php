<?php
include('connection.php');

//session set
session_start();

if(isset($_REQUEST["filters"])){

    //request array data met skill filters in array zetten
    $filters = json_decode(stripslashes($_REQUEST['filters']));

    //session array aanmaken met filters 
    $_SESSION['skillArray'] =  $filters;

    //Wanneer geen page value, dan is pagina 1    
    if(isset($_REQUEST["page"])) { $page  = $_REQUEST["page"]; } else { $page=1; };  
    $start_from = ($page-1) * $limit;  
     $_SESSION['page'] =  $page;
     $_SESSION['fontys'] =  $_REQUEST["fontys"];
     $_SESSION['nonFontys'] =  $_REQUEST["nonFontys"];
     $_SESSION['paid'] =  $_REQUEST["paid"];
     $_SESSION['voluntary'] =  $_REQUEST["voluntary"];

    //IF GEEN FILTERS IN FILTERARRAY---------------------
    //als er geen filters zijn meegeven in het filter array, start dan de query die alle Unite projecten in laadt.
    if (empty($filters)) {
        if($_REQUEST["fontys"] && !$_REQUEST["nonFontys"]){
             if($_REQUEST["voluntary"] && !$_REQUEST["paid"]){
                $sqlC = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE (p_voluntary = 1) AND (p_fontys = 1)";
                $sql = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE (p_voluntary = 1) AND (p_fontys = 1) LIMIT $start_from, $limit";
             }
             if(!$_REQUEST["voluntary"] && $_REQUEST["paid"]){
                $sqlC = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE (p_paid = 1) AND (p_fontys = 1)";
                $sql = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE (p_paid = 1) AND (p_fontys = 1) LIMIT $start_from, $limit";
             }
            if(!$_REQUEST["voluntary"] && !$_REQUEST["paid"]){
                $sqlC = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE (p_voluntary = 1 OR p_paid = 1) AND (p_fontys = 1)";
                $sql = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE (p_voluntary = 1 OR p_paid = 1) AND (p_fontys = 1) LIMIT $start_from, $limit";              
             }
            if($_REQUEST["voluntary"] && $_REQUEST["paid"]){
                $sqlC = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE (p_voluntary = 1 OR p_paid = 1) AND (p_fontys = 1) AND (p_other = 0)";
                $sql = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE (p_voluntary = 1 OR p_paid = 1) AND (p_fontys = 1) AND (p_other = 0) LIMIT $start_from, $limit";
             }
        }

        if(!$_REQUEST["fontys"] && $_REQUEST["nonFontys"]){

             if($_REQUEST["voluntary"] && !$_REQUEST["paid"]){
                $sqlC = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE (p_voluntary = 1) AND (p_other = 1)";
                $sql = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE (p_voluntary = 1) AND (p_other = 1) LIMIT $start_from, $limit";
             }
             if(!$_REQUEST["voluntary"] && $_REQUEST["paid"]){
                $sqlC = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE (p_paid = 1) AND (p_other = 1) ";
                $sql = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE (p_paid = 1)  AND (p_other = 1) LIMIT $start_from, $limit";
             }
            if(!$_REQUEST["voluntary"] && !$_REQUEST["paid"]){
                $sqlC = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE (p_voluntary = 1 OR p_paid = 1) AND (p_other = 1)";
                $sql = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE (p_voluntary = 1 OR p_paid = 1) AND (p_other = 1) LIMIT $start_from, $limit";            
             }
            if($_REQUEST["voluntary"] && $_REQUEST["paid"]){
                $sqlC = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE (p_voluntary = 1 OR p_paid = 1) AND (p_other = 1)";
                $sql = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE (p_voluntary = 1 OR p_paid = 1) AND (p_other = 1) LIMIT $start_from, $limit";
             }
        }

        //Wanneer FONTYS/NONFONTYS niet is gedefineerd
        if( (!$_REQUEST["fontys"] && !$_REQUEST["nonFontys"]) || ($_REQUEST["fontys"] && $_REQUEST["nonFontys"]) ){
             if($_REQUEST["voluntary"] && !$_REQUEST["paid"]){
                $sqlC = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE (p_voluntary = 1)";
                $sql = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE (p_voluntary = 1) LIMIT $start_from, $limit";
             }
             if(!$_REQUEST["voluntary"] && $_REQUEST["paid"]){
                $sqlC = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE (p_paid = 1)";
                $sql = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE (p_paid = 1) LIMIT $start_from, $limit";
             }
            if(!$_REQUEST["voluntary"] && !$_REQUEST["paid"]){
                $sqlC = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects ";
                $sql = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects LIMIT $start_from, $limit";            
             }
            if($_REQUEST["voluntary"] && $_REQUEST["paid"]){
                $sqlC = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects";
                $sql = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects LIMIT $start_from, $limit";
             }
        }


    //results count query
    $resultC=mysqli_query($link, $sqlC);

    //tellen results vorige query
    $aantalResultsNaProjectFilters = mysqli_num_rows($resultC);

    //Fetch results 
    $result=mysqli_query($link, $sql);
    ?>
        
         <span class="aantalResults"><?php echo $aantalResultsNaProjectFilters; ?> Results</span>
         <div class="alleProjCont"><p class="alleProj">Alle Unite projecten</p></div><br>
         <hr class="hr1">
        <?php

    //counter voor while loop extra class toevoegen
    $counter = 0;

    //toon resultaten
    while($row=mysqli_fetch_array($result))
    {
            $your_var = $row['p_id'];
        ?>
        <a href="projectdetails.php?proj=<?php echo $row['p_id']; ?>">
        <div class="clearfix card cardd <?php if($counter == 0){echo "carddFirst"; $counter ++;} ?>" <?php echo 'id="' . $row['p_id'] . '"'; ?>  style="width: 30%;">
        <div class="card-body">
          <h4 class="card-title"><?php echo $row['p_name']?></h4>
          <p class="card-text"><?php echo $row['p_location']?></p>
        </div>

        <div class="cardSplitCont">
                <div class="card-footer split2">
                    <!-- display project skills -->
                    <?php
                    $sql2 = "SELECT skill FROM skills WHERE p_id = '$your_var'";

                    $result2=mysqli_query($link, $sql2);

                    while($row2=mysqli_fetch_array($result2))
                    {
                        echo '<a href="#" class="btn btn-primary">' . $row2['skill'] . '</a>';
                    }
                    ?>
                </div>


                <div class="card-footer split1">
                 <?php if($row['p_fontys'] == 1){echo '<a href="#" class="btn btn-primary">Fontys project</a>';}
                 if($row['p_other'] == 1){echo '<a href="#" class="btn btn-primary">non-Fontys project</a>';}
                 if($row['p_voluntary'] == 1){echo '<a href="#" class="btn btn-primary">voluntary</a>';}
                 if($row['p_paid'] == 1){echo '<a href="#" class="btn btn-primary">paid</a>';}
                 ?>

                </div>


        </div>
        <!-- end of cardSplitCont -->


        </div>  
         </a>

        <?php
    }   




     //aantal paginas berekenen voor pagination element hieronder
    $total_pages = ceil($aantalResultsNaProjectFilters / $limit);
    echo '<hr class="hrBottom"><br>';
    ?>

    <ul class='pagination' id="pagination">
        <?php if(!empty($total_pages)):for($i=1; $i<=$total_pages; $i++):  
         if($i == $_REQUEST["page"]):?>
            <li class='page-item active'  id="<?php echo $i;?>"><span class="page-link"><?php echo $i;?></span></li> 
         <?php else:?>
         <li id="<?php echo $i;?>" class="page-item"><span class="page-link" class="page-link"><?php echo $i;?></span></li>
         <?php endif;?> 
        <?php endfor;endif;?> 
    </ul>
</div>

<?php
}


    //IF WEL FILTERS IN FILTERARRAY---------------------
    else{
        //alle results van skill in array stoppen en wanneer bijhorend project er al in zit, niet toevoegen
        //elke quoted string escapen tegen sql injectie
        foreach($filters as $key => $val) {
          $filters[$key] = mysqli_real_escape_string($link, $val);
        }

        //plaatsts haakjes om de values (' ',' ')
        $in_str = "'".implode("', '", $filters)."'"; 

        $sql = "SELECT p_id FROM skills WHERE skill IN ($in_str)";

        //resultaten ophalen
        $result=mysqli_query($link, $sql);

        //aanmaken van een array waarin alleen values in komen die nog niet erin zijn gestopt om tegen te gaan dat er meerdere 'UX designs worden getoont'
        $sameProjects = array();

        //resultaten tonen
        while($row=mysqli_fetch_array($result))
        {
        if(in_array($row["p_id"], $sameProjects)){

        }else{

            array_push($sameProjects, $row["p_id"]);
        }
            $projectIDs = $sameProjects;
        }
        
        foreach($projectIDs as $key => $val) {
          $projectIDs[$key] = mysqli_real_escape_string($link, $val);
        }

        //plaatsts haakjes om de values (' ',' ')
        $in_str2 = "'".implode("', '", $projectIDs)."'"; 


        if($_REQUEST["fontys"] && !$_REQUEST["nonFontys"]){
             if($_REQUEST["voluntary"] && !$_REQUEST["paid"]){
                $sqlC = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE p_id IN ($in_str2) AND (p_voluntary = 1) AND (p_fontys = 1)";
                $sql = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE p_id IN ($in_str2) AND (p_voluntary = 1) AND (p_fontys = 1) LIMIT $start_from, $limit";
             }
             if(!$_REQUEST["voluntary"] && $_REQUEST["paid"]){
                $sqlC = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE p_id IN ($in_str2) AND (p_paid = 1) AND (p_fontys = 1)";
                $sql = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE p_id IN ($in_str2) AND (p_paid = 1) AND (p_fontys = 1) LIMIT $start_from, $limit";
             }
            if(!$_REQUEST["voluntary"] && !$_REQUEST["paid"]){
                $sqlC = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE p_id IN ($in_str2) AND (p_voluntary = 1 OR p_paid = 1) AND (p_fontys = 1)";
                $sql = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE p_id IN ($in_str2) AND (p_voluntary = 1 OR p_paid = 1) AND (p_fontys = 1) LIMIT $start_from, $limit";              
             }
            if($_REQUEST["voluntary"] && $_REQUEST["paid"]){
                $sqlC = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE p_id IN ($in_str2) AND (p_voluntary = 1 OR p_paid = 1) AND (p_fontys = 1) AND (p_other = 0)";
                $sql = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE p_id IN ($in_str2) AND (p_voluntary = 1 OR p_paid = 1) AND (p_fontys = 1) AND (p_other = 0) LIMIT $start_from, $limit";
             }
        }

        if(!$_REQUEST["fontys"] && $_REQUEST["nonFontys"]){

             if($_REQUEST["voluntary"] && !$_REQUEST["paid"]){
                $sqlC = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE p_id IN ($in_str2) AND (p_voluntary = 1) AND (p_other = 1)";
                $sql = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE p_id IN ($in_str2) AND (p_voluntary = 1) AND (p_other = 1) LIMIT $start_from, $limit";
             }
             if(!$_REQUEST["voluntary"] && $_REQUEST["paid"]){
                $sqlC = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE p_id IN ($in_str2) AND (p_paid = 1) AND (p_other = 1) ";
                $sql = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE p_id IN ($in_str2) AND (p_paid = 1)  AND (p_other = 1) LIMIT $start_from, $limit";
             }
            if(!$_REQUEST["voluntary"] && !$_REQUEST["paid"]){
                $sqlC = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE p_id IN ($in_str2) AND (p_voluntary = 1 OR p_paid = 1) AND (p_other = 1)";
                $sql = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE p_id IN ($in_str2) AND (p_voluntary = 1 OR p_paid = 1) AND (p_other = 1) LIMIT $start_from, $limit";            
             }
            if($_REQUEST["voluntary"] && $_REQUEST["paid"]){
                $sqlC = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE p_id IN ($in_str2) AND (p_voluntary = 1 OR p_paid = 1) AND (p_other = 1)";
                $sql = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE p_id IN ($in_str2) AND (p_voluntary = 1 OR p_paid = 1) AND (p_other = 1) LIMIT $start_from, $limit";
             }
        }

        //Wanneer FONTYS/NONFONTYS niet is gedefineerd
        if( (!$_REQUEST["fontys"] && !$_REQUEST["nonFontys"]) || ($_REQUEST["fontys"] && $_REQUEST["nonFontys"]) ){
             if($_REQUEST["voluntary"] && !$_REQUEST["paid"]){
                $sqlC = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE p_id IN ($in_str2) AND (p_voluntary = 1)";
                $sql = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE p_id IN ($in_str2) AND (p_voluntary = 1) LIMIT $start_from, $limit";
             }
             if(!$_REQUEST["voluntary"] && $_REQUEST["paid"]){
                $sqlC = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE p_id IN ($in_str2) AND (p_paid = 1)";
                $sql = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE p_id IN ($in_str2) AND (p_paid = 1) LIMIT $start_from, $limit";
             }
            if(!$_REQUEST["voluntary"] && !$_REQUEST["paid"]){
                $sqlC = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE p_id IN ($in_str2)";
                $sql = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE p_id IN ($in_str2) LIMIT $start_from, $limit";            
             }
            if($_REQUEST["voluntary"] && $_REQUEST["paid"]){
                $sqlC = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE p_id IN ($in_str2)";
                $sql = "SELECT p_name, p_created, p_location, p_owner, p_fontys, p_other, p_paid, p_voluntary, p_id FROM projects WHERE p_id IN ($in_str2) LIMIT $start_from, $limit";
             }
        }



        // SKILLS OPHALEN van projecten uit str2
        $sql2 = "SELECT skill FROM skills WHERE p_id IN ($in_str2)";



        //Fetch results COUNT PROJECT FILTERS
        $resultC=mysqli_query($link, $sqlC);


    //tellen results
    $aantalResultsNaProjectFilters = mysqli_num_rows($resultC);

        //Fetch results LIMITED 
        $result=mysqli_query($link, $sql);

         ?>
       
         <span class="aantalResults"><?php echo $aantalResultsNaProjectFilters; ?> Results</span>
         <hr class="hr1">
        <?php


        if(mysqli_num_rows($result) === 0){
            echo " ";
        }
        else{

            //counter voor while loop extra class toevoegen
    $counter = 0;

    //Display results
    while($row=mysqli_fetch_array($result))
    {

            $your_var = $row['p_id'];
        ?>
        <a href="projectdetails.php?proj=<?php echo $row['p_id']; ?>">
        <div class="clearfix card cardd <?php if($counter == 0){echo "carddFirst"; $counter ++;} ?>" <?php echo 'id="' . $row['p_id'] . '"'; ?>  style="width: 30%;">
        <div class="card-body">
          <h4 class="card-title"><?php echo $row['p_name']?></h4>
          <p class="card-text"><?php echo $row['p_location']?></p>
        </div>

        <div class="cardSplitCont">
                <div class="card-footer split2">
                    <!-- display project skills -->
                    <?php
                    $sql2 = "SELECT skill FROM skills WHERE p_id = '$your_var'";
                    //Fetch results 
                    $result2=mysqli_query($link, $sql2);

                    while($row2=mysqli_fetch_array($result2))
                    {
                        echo '<a href="#" class="btn btn-primary">' . $row2['skill'] . '</a>';
                    }
                    ?>
                </div>


                <div class="card-footer split1">
                 <?php if($row['p_fontys'] == 1){echo '<a href="#" class="btn btn-primary">Fontys project</a>';}
                 if($row['p_other'] == 1){echo '<a href="#" class="btn btn-primary">non-Fontys project</a>';}
                 if($row['p_voluntary'] == 1){echo '<a href="#" class="btn btn-primary">voluntary</a>';}
                 if($row['p_paid'] == 1){echo '<a href="#" class="btn btn-primary">paid</a>';}
                 ?>

                </div>


        </div>
        <!-- end of cardSplitCont -->


        </div>  
         </a>

        <?php
    }   


     //paginas tellen
    $total_pages = ceil($aantalResultsNaProjectFilters / $limit);
    echo '<hr class="hrBottom"><br>';

    ?>

    <?php
    // PAGINATION TOEVOEGEN, telt eerst het aantal paginas 
    // tellen van aantal results, waarna die worden gedeeld door het aantal te tonen resulaten per pagina = $limit
    $aantalResultsNaProjectFilters = mysqli_num_rows($resultC);
    $total_pages = ceil($aantalResultsNaProjectFilters / $limit);
    ?>

    <ul class='pagination' id="pagination">
        <?php if(!empty($total_pages)):for($i=1; $i<=$total_pages; $i++):  
         if($i == $_REQUEST["page"]):?>
            <li class='page-item active'  id="<?php echo $i;?>"><span class="page-link"><?php echo $i;?></span></li> 
         <?php else:?>
         <li id="<?php echo $i;?>" class="page-item"><span class="page-link" class="page-link"><?php echo $i;?></span></li>
         <?php endif;?> 
        <?php endfor;endif;?> 
    </ul>

<?php
    }

}
}
 
// close connection
mysqli_close($link);
?>


